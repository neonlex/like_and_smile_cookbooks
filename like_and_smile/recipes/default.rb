template "#{deploy[:deploy_to]}/current/config/asin.yml" do
  source "asin.yml.erb"
  mode "0660"
  group deploy[:group]
  owner deploy[:user]
  variables :access_key_id => node[:like_and_smile][:asin][:access_key_id],
            :secret_access_key => node[:like_and_smile][:asin][:secret_access_key]

  only_if do
    File.directory?("#{deploy[:deploy_to]}/current")
  end
end

template "#{deploy[:deploy_to]}/current/config/resque_auth.yml" do
  source "resque_auth.yml.erb"
  mode "0660"
  group deploy[:group]
  owner deploy[:user]
  variables :user => node[:like_and_smile][:resque][:user],
            :password => node[:like_and_smile][:resque][:password]

  only_if do
    File.directory?("#{deploy[:deploy_to]}/current")
  end
end
